# **TIP ARTIST**

[![Created Badge](https://badges.pufler.dev/created/teustoscano/blockchain-tip-artist)](https://badges.pufler.dev) 

*Blockchain project based on superHi course.*

<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" title="JavaScript"><img src="https://github.com/get-icon/geticon/raw/master/icons/javascript.svg" alt="JavaScript" width="21px" height="21px"></a>
<a href="https://www.w3.org/TR/CSS/" title="CSS3"><img src="https://github.com/get-icon/geticon/raw/master/icons/css-3.svg" alt="CSS3" width="21px" height="21px"></a>
<a href="https://www.w3.org/TR/html5/" title="HTML5"><img src="https://github.com/get-icon/geticon/raw/master/icons/html-5.svg" alt="HTML5" width="21px" height="21px"></a>
<a href="https://www.w3.org/TR/html5/" title="Metamask"><img src="https://cdn.iconscout.com/icon/free/png-256/metamask-2728406-2261817.png" alt="Metamask" width="21px" height="21px"></a>

![print screen](assets/asset1.png)
![print screen](assets/asset2.png)

