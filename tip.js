const web3 = new Web3(Web3.givenProvider)

const form = document.querySelector("form")

const send = async function (amount) {
    const accounts = await window.ethereum.request({ method: "eth_requestAccounts" })

    const wei = web3.utils.toWei(amount, "ether")

    if (accounts.length > 0) {
        window.ethereum.request({
            method: "eth_sendTransaction",
            params: [{
                from: accounts[0],
                to: "0x683d48cf68F4D51966E7a54F78071D493d0f05a3",
                value: web3.utils.toHex(wei)
            }]
        })
    }
    // alert("ETH: " + amount);
}

// Check if has metamask installed on browser
if (window.ethereum) {
    form.classList.add("has-eth")
}

form.addEventListener("submit", function (event) {
    event.preventDefault();

    if (window.ethereum) {
        const input = form.querySelector("input")
        send(input.value);
    } else {

    }
})